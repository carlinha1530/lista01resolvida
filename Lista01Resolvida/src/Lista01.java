/**
 * 
 * A resolucao da lista eh individual. Caso seja identificada "cola", 
 * todos os envolvidos terao a nota da lista zerada.
 *
 */
public class Lista01 {

	/**
	 * 
	 * Encontre o maior elemento do vetor e o retorne.
	 * 
	 */
	public static int maiorElementoVetor(int[] vetor) {
		int valorMaior = vetor[0];
		
		for (int i = 0; i < vetor.length; i++) { //percorre vetor
			if (vetor[i] > valorMaior) {
				valorMaior = vetor[i];
			}
		}
		return valorMaior;
	}

	/**
	 * 
	 * Encontre o maior elemento da matriz e o retorne.
	 * 
	 */
	public static int maiorElementoMatriz(int[][] matriz) {
		int valorMaior = matriz[0][0];
		
		for (int i = 0; i < matriz.length; i++) {
			for (int j = 0; j < matriz[i].length; j++) {
				if (matriz[i][j] > valorMaior) {
					valorMaior = matriz[i][j];
				}
			}
		}
		
		return valorMaior;
	}

	/**
	 * 
	 * Defina um vetor com o valor do primeiro parametro e inicialize
	 * todas as posicoes deste vetor com o segundo parametro.
	 * 
	 */
	public static int[] criaVetor(int tamanhoVetor, int valorInicial) {
		int[] novoVetor = new int[tamanhoVetor];
		
		for (int i = 0; i < novoVetor.length; i++) {
			novoVetor[i] = valorInicial;
		}
		
		return novoVetor;
	}

	/**
	 * 
	 * Dado o vetor de registros do tipo pessoa, retorna o nome daquela 
	 * que tem a maior idade (o mais velho).
	 * 
	 */
	public static String recuperarNomeMaisVelha(Pessoa[] pessoas) {
		String nomeMaisVelho = pessoas[0].nome;
		int idadeMaisVelho = pessoas[0].idade;
		
		for (int i = 0; i < pessoas.length; i++) {
			if (pessoas[i].idade > idadeMaisVelho) {
				idadeMaisVelho = pessoas[i].idade;
				nomeMaisVelho = pessoas[i].nome;
			}
		}
		
		return nomeMaisVelho;
	}

	/**
	 * 
	 * Dado o vetor de registros do tipo pessoa, calcular a media das 
	 * idades de todas as pessoas do conjunto.
	 * 
	 */
	public static double calcularMediaIdade(Pessoa[] pessoas) {
		int somaIdades = 0;
		
		for (int i = 0; i < pessoas.length; i++) {
			somaIdades = somaIdades + pessoas[i].idade;
		}
		
		double media = somaIdades / pessoas.length;
		
		return media;
	}

	/**
	 * 
	 * Verifica se o nome buscado existe no vetor de pessoas passado como
	 * argumento de entrada. 
	 * 
	 * Dica: pesquise pela funcao equals().
	 * 
	 */
	public static boolean existePessoaVetor(String nomeBuscado, Pessoa[] pessoas) {
		boolean existe = false;

		for (int i = 0; i < pessoas.length; i++) {
			if (pessoas[i].nome.equals(nomeBuscado)) {
				existe = true;
			}
		}
		
		return existe;
		
		/* 
		 * MANEIRA ALTERNATIVA
		 * 
		return existe;
		
		boolean existe = false;

		for (int i = 0; i < pessoas.length; i++) {
			if (pessoas[i].nome.equals(nomeBuscado)) {
				existe = true;
			}
		}
		
		return existe; 
		*/
	}

}
